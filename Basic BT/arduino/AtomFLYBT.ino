#include <M5Atom.h>
#include "AtomFly.h"
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <BLE2902.h>

int motorA = 0;
int motorB = 0;
int motorC = 0;
int motorD = 0;

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

#define SERVICE_UUID        "3afd098a-2111-46a9-84be-8bec1a96ea64"
#define CHARACTERISTIC_UUID "d4d45719-3d11-494a-a69a-4c06af8a09b8"

BLEServer* pServer = NULL;
BLECharacteristic* pCharacteristic = NULL;
bool deviceConnected = false;

class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      Serial.println("BT connect");
      deviceConnected = true;
    };

    void onDisconnect(BLEServer* pServer) {
      Serial.println("BT disconnect");
      deviceConnected = false;
    }
};

class MyCallbacks: public BLECharacteristicCallbacks {
  void onRead(BLECharacteristic *pCharacteristic) {
    Serial.println("BT read");
    pCharacteristic->setValue("Hello World!");
  }
  
  void onWrite(BLECharacteristic *pCharacteristic) {
    Serial.println("BT write");
    String value = pCharacteristic->getValue().c_str();
    // Length (with one extra character for the null terminator)
    int str_len = value.length() + 1; 
    // Prepare the character array (the buffer) 
    char char_array[str_len];
    // Copy it over 
    value.toCharArray(char_array, str_len);

    int n = sscanf(char_array, "%d;%d;%d;%d", &motorA, &motorB, &motorC, &motorD);
  }
};

CRGB led(0, 0, 0);
CRGB HSVtoRGB(uint16_t h, uint16_t s, uint16_t v);

double pitch, roll;
String pitchString, rollString;

double Calibrationbuff[3];
double r_rand = 180 / PI;
void setup()
{
    // Start Serial
    Serial.begin(115200);
  
    M5.begin(true, false, true);

    Calibrationbuff[0] = 0;
    Calibrationbuff[1] = 0;
    Calibrationbuff[2] = 0;

    fly.begin();
    if( fly.initFly() != 0)
    {
        Serial.println("failed");
        led = CRGB(0,255,0);
        M5.dis.drawpix(0, led);
        while(1)
        {
            delay(100);
        }
    }

    Serial.println("OK");
    led = CRGB(255,0,0);
    M5.dis.drawpix(0, led);
   
    Calibrationbuff[0] = Calibrationbuff[0] / 36;
    Calibrationbuff[1] = Calibrationbuff[1] / 36;

    Serial.printf("Calibration:%.2f,%.2f\n", Calibrationbuff[0], Calibrationbuff[1]);

    led = HSVtoRGB(0, 0, 100);
    M5.dis.drawpix(0, led);

    // Bluetoothh setup and start
    BLEDevice::init("atomfly");
  BLEServer *pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());
  BLEService *pService = pServer->createService(SERVICE_UUID);
  pCharacteristic = pService->createCharacteristic(
                                         CHARACTERISTIC_UUID,
                                         BLECharacteristic::PROPERTY_READ |
                                         BLECharacteristic::PROPERTY_WRITE |
                                         BLECharacteristic::PROPERTY_NOTIFY |
                                         BLECharacteristic::PROPERTY_INDICATE
                                       );
  pCharacteristic->setCallbacks(new MyCallbacks());
  pCharacteristic->addDescriptor(new BLE2902());

  pService->start();
  BLEAdvertising *pAdvertising = pServer->getAdvertising();
  pAdvertising->start();
}

uint8_t mode = 0;
uint16_t count = 0;
void loop()
{
    uint16_t tofData = fly.readTOF();
    tofData = (tofData > 500) ? 500 : tofData;

    fly.getAttitude(&pitch, &roll);
    double arc = atan2(pitch, roll) * r_rand + 180;
    double val = sqrt(pitch * pitch + roll * roll);
    //Serial.printf("%.2f,%.2f,%.2f,%.2f\n", pitch, roll, arc, val);
    pitchString = String(pitch, 4);
    
    fly.WritePWM(AtomFly::kMotor_A, motorA);
    fly.WritePWM(AtomFly::kMotor_B, motorB);
    fly.WritePWM(AtomFly::kMotor_C, motorC);
    fly.WritePWM(AtomFly::kMotor_D, motorD);

    if (deviceConnected) {
      if(M5.Btn.wasPressed()) {
        Serial.println("Button B pressed!");
        pCharacteristic->setValue("Button B pressed!");
        pCharacteristic->notify();
      }
    }

    M5.update();
    delay(10);
}

// R,G,B from 0-255, H from 0-360, S,V from 0-100

CRGB HSVtoRGB(uint16_t h, uint16_t s, uint16_t v)
{
    CRGB ReRGB(0, 0, 0);
    int i;
    float RGB_min, RGB_max;
    RGB_max = v * 2.55f;
    RGB_min = RGB_max * (100 - s) / 100.0f;

    i = h / 60;
    int difs = h % 60;
    float RGB_Adj = (RGB_max - RGB_min) * difs / 60.0f;

    switch (i)
    {
    case 0:

        ReRGB.r = RGB_max;
        ReRGB.g = RGB_min + RGB_Adj;
        ReRGB.b = RGB_min;
        break;
    case 1:
        ReRGB.r = RGB_max - RGB_Adj;
        ReRGB.g = RGB_max;
        ReRGB.b = RGB_min;
        break;
    case 2:
        ReRGB.r = RGB_min;
        ReRGB.g = RGB_max;
        ReRGB.b = RGB_min + RGB_Adj;
        break;
    case 3:
        ReRGB.r = RGB_min;
        ReRGB.g = RGB_max - RGB_Adj;
        ReRGB.b = RGB_max;
        break;
    case 4:
        ReRGB.r = RGB_min + RGB_Adj;
        ReRGB.g = RGB_min;
        ReRGB.b = RGB_max;
        break;
    default: // case 5:
        ReRGB.r = RGB_max;
        ReRGB.g = RGB_min;
        ReRGB.b = RGB_max - RGB_Adj;
        break;
    }

    return ReRGB;
}
